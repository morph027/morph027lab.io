+++
title = "Git log/history in JSON or YAML format"
description = ""
author = "morph027"
tags = ["git"]
date = "2020-07-04T10:00:00+02:00"
+++

You can use git [pretty formats](https://git-scm.com/docs/pretty-formats) for creating YAML log (YAML instead of JSON, no messing around with trailing comma).

Example:


```bash
echo "commits:"
git \
  --no-pager log \
  --since "7 days ago" \
  --pretty=format:"  - id: %h%n    signer_key: '%GK'%n    timestamp: '%at'%n    subject: '%f'%n    body: |%n      %(trailers:separator=\n      )"
```

This shows all git commits from last 7 days with their signer key, timestamp, (sanitized) subject and the body as YAML. See [pretty formats](https://git-scm.com/docs/pretty-formats) docs for more fields.

If you want to use JSON, you can convert the YAML e.g. using [yq](https://github.com/mikefarah/yq):

```bash
echo "commits:"
git \
  --no-pager log \
  --since "7 days ago" \
  --pretty=format:"  - id: %h%n    signer_key: '%GK'%n    timestamp: '%at'%n    subject: '%f'%n    body: |%n      %(trailers:separator=\n      )" | yq '.'
```
