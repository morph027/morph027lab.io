+++
author = ""
date = "2016-03-24T08:45:21+02:00"
description = ""
tags = []
title = "GPG"

+++

> Mail: ```morphsen<-at->gmx.com```
>
> ID: ```E6122FF7``` (@[Keyserver](https://sks-keyservers.net/pks/lookup?op=vindex&search=0XE6122FF7&fingerprint=on))
>
> Fingerprint: ```7770 3557 C89C 6454 23AB  6696 6962 5AD1 E612 2FF7``` 

If you want to know more about GPG, have a look at [ArnoldArts](http://www.arnoldarts.de/gpg/) page.
