+++
date = "2013-09-05"
title = "SAMBA4 kerberos keytab management"
tags = ["samba4"]

+++

In case you'll need another keytab for kerberos binding (e.g. [mod_auth_kerb](http://modauthkerb.sourceforge.net/), creating and exporting keytabs can be done like this

## Random Password

We do not need it later, it's just necessary for importing the record.

```bash
python
```

```python
import base64
base64.b64encode('myRandomPassword'.encode('utf-16-le'))
'MgAzAFcAawBhADUAdgBtAHoAagA='
```

Exit with CTRL+D

## 	LDIF for principal (if new one)

```bash
$ cat > /tmp/PRINCIPAL.ldif << EOF
dn: CN=HOSTNAME,CN=Users,DC=example,DC=com
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: user
description: Service Account for #######
userAccountControl: 66048
accountExpires: 9223372036854775807
sAMAccountName: HOSTNAME
servicePrincipalName: SERVICETYPE/HOSTNAME.lan.example.com
clearTextPassword:: HASH-FROM-ABOVE
EOF
```

## Import principal

```bash
$ ldbadd -H ldap://DOMAINCONTROLLER -v -k yes /tmp/PRINCIPAL.ldif
```

## Export keytab

```bash
$ samba-tool domain exportkeytab /tmp/PRINCIPAL.keytab  --principal=PRINCIPAL
```

## Check

```bash
$ kinit -V -k -t /tmp/PRINCIPAL.keytab

Using default cache: /tmp/krb5cc_0
Using principal: PRINCIPAL@EXAMPLE.COM
Using keytab: /tmp/PRINCIPAL.keytab
Authenticated to Kerberos v5
```
