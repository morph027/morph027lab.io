+++
title = "Debian repository hosting using Gitlab Pages"
description = ""
author = "morph027"
tags = ["gitlab"]
date = "2018-11-25T08:00:00+02:00"
+++

If you're eagerly building your own software and packages (or like me, do some packaging for others), you'll need somw sort of webspace to make them available to the public.

And if you're using Gitlab CI (and you should) to build you packages, you can easily add some steps to let [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html) take care of the repo hosting.

Here's an [example](https://gitlab.com/morph027/repo-hosting-on-gitlab) project which just downloads some random packages and then create the repo. Have fun!
