+++
date = "2015-05-25"
tags = ["docker"]
title = "Deploy a private Docker registry including mirror and web frontend"

+++

---
**UPDATE 2016-02-29**

* Adjusted to registry v2
* added web frontend

---

##Server

Start the [registry](https://docs.docker.com/registry/):

```
docker run -d -p 5000:5000 --restart=always --name docker-registry -v /var/lib/docker-registry:/var/lib/registry registry:2
```

Start  the [mirror](https://docs.docker.com/v1.7/articles/registry_mirror/):

```
docker run -d -p 5555:5000 --restart=always --name docker-mirror -v /var/lib/docker-mirror:/var/lib/registry -e STORAGE_PATH=/var/lib/registry -e STANDALONE=false -e MIRROR_SOURCE=https:/registry-1.docker.io -e MIRROR_SOURCE_INDEX=https://index.docker.io registry
```

Start the [web frontend](https://github.com/kwk/docker-registry-frontend):

```
docker run -d --restart=always --name docker-registry-frontend -e ENV_DOCKER_REGISTRY_HOST=docker-registry.hq.packetwerk.com -e ENV_DOCKER_REGISTRY_PORT=5000 -e ENV_DOCKER_REGISTRY_USE_SSL=1 -p 80:80 konradkleine/docker-registry-frontend:v2
```
	
##Client

Edit _/etc/default/docker_ and add _DOCKER\_OPTS_ like this:

> DOCKER\_OPTS="--dns=ns1 --dns=ns2 --insecure-registry=docker-registry:5000 --registry-mirror=http://docker-registry:5555"
