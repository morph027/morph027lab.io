+++
date = "2016-09-15T09:00:00+01:00"
tags = ["omd", "checkmk", "mariadb"]
title = "mysql-server meta package for mariadb-server"

+++

OMD (and probably a lot of other packages)  depends on ```mysql-server```, so we can't just use ```mariadb-server``` easily. Or can we?

Sure, we can rebuild the whole OMD thing with different dependencies, which will take a while. Or, we can just create a meta-package called ```mysql-server```, which just depends on ```mariadb-server``` and satisfies the dependency for OMD.

I do not care about packaging hell, i just use [fpm](https://github.com/jordansissel/fpm), which requires ruby. Also i do not want to mess up my system, so i just use docker.
 
```
mkdir ~/omd
docker run  --rm -ti -v ~/omd:/build -w /build ruby:alpine /bin/sh
apk --no-cache add build-base libffi-dev ruby-dev tar
gem install fpm --no-ri --no-rdoc
VERSION=$(curl -s http://ftp.hosteurope.de/mirror/mariadb.org/repo/10.1/debian/dists/jessie/main/binary-amd64/Packages.gz | zcat | awk '/^Package: mariadb-server$/,/^Version:/' | tail -n1 | awk '{print $NF}')
fpm -s dir -t deb --name mysql-server --version "$VERSION" --vendor MariaDB --maintainer "morph027 <morphsen@gmx.com>" --depends mariadb-server --description "meta-package for mariadb-server" --url "http://mariadb.org/" .
```
