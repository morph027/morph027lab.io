+++
date = "2016-02-05"
tags = ["zfs"]
title = "ZFS on Linux and Check_MK zfs_arc_cache"

+++

To allow Check\_MK to inventorize and fetch data for the [zfs\_arc\_cache](https://mathias-kettner.de/checkmk_check_zfs_arc_cache.html) plugin, just create a file like _/usr/lib/check\_mk\_agent/plugins/zfs\_arc\_cache_ with the content from below and make it executable.

```
#!/bin/sh
echo "<<<zfs_arc_cache>>>"
cat /proc/spl/kstat/zfs/arcstats | tail -n +3 | awk '{print $1" = "$NF}'
```
		
You then should get some nice graphs showing all the stats.

![ZFS ARC cache in Check_MK](/zfs-arc.png)
