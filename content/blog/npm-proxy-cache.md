+++
date = "2016-04-27T09:00:00+01:00"
tags = ["npm", "ci", "nodejs"]
title = "NPM proxy cache for CI"

+++

Same like [bower](/post/bower-caching-proxy/), it might happen that you need to build things not only locally but also in some sort of CI. Deploying a [npm proxy cache](https://www.npmjs.com/package/npm-proxy-cache) is fairly easy.

## Installation

I suggest to add a specific user for this:

```bash
useradd -m -d /var/cache/npm -s /bin/false npm
```

Some neccessary packages:

```bash
apt-get install nodejs
```

You probably want to fetch the [latest](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) packages.

Now we install the cache:

```bash
sudo -u npm /bin/bash
cd
npm install npm-proxy-cache
```

## Startup

### _/etc/systemd/system/npm-proxy-cache.service_

```
[Unit]
Description=npm proxy cache
After=network.target
After=syslog.target

[Service]
RuntimeDirectory=npm-proxy-cache
PIDFile=/var/run/npm-proxy-cache/npm-proxy-cache.pid
WorkingDirectory=/var/cache/npm
ExecStart=/var/cache/npm/node_modules/npm-proxy-cache/bin/npm-proxy-cache -e -h %H
Restart=always
RestartSec=10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=npm-proxy-cache
User=npm
Group=npm

[Install]
WantedBy=multi-user.target
```

## Client usage

```bash
npm config set proxy http://npm-proxy-cache.example.com:8080
npm config set https-proxy http://npm-proxy-cache.example.com:8080
npm config set strict-ssl false
```
