+++
date = "2016-03-14"
tags = ["aptly"]
title = "Protect aptly API with basic authentication"

+++

You should really protect your aptly API with at least basic authentication. This is easy to achive with a reverse proxy webserver like nginx or Apache. As you are going to transfer credentials then, you should also protect the whole thing with [SSL](https://letsencrypt.org/getting-started/).

## nginx

Example snippet (most of this is misc. nginx and ssl setup, interesting bits tagged with ```###```):

```nginx
server {
	listen 80;
	server_name your.repo.org ;
	
	### rewrite all non https traffic
	location /api/ {
		rewrite ^/(.*)$ https://$server_name$request_uri permanent;  # enforce https
	}
	root /nowhere;
}
server {
	server_name your.repo.org;
	ssl on;
	listen 443 ssl http2;
	ssl_certificate /etc/letsencrypt/live/repo.org/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/repo.org/privkey.pem;
	ssl_protocols SSLv3 TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:ECDHE-RSA-RC4-SHA:ECDHE-ECDSA-RC4-SHA:AES128:AES256:RC4-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!3DES:!MD5:!PSK;
	ssl_prefer_server_ciphers on;
	root /nowhere;
	location ~ /\.ht {
		deny all;
	}
	### protect /api with basic auth
	location /api/ {
		client_max_body_size 100M;
		auth_basic "Restricted";
		auth_basic_user_file /etc/nginx/.htpasswd.aptly;
		proxy_redirect	off;
		proxy_pass	http://localhost:8080/api/;
		proxy_redirect	http://localhost:8080/api/ /api;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header Host $http_host;
		proxy_set_header Origin "";
	}
}
```
	
You can create a _.htpasswd_ file using

```bash
apt-get install apache2-utils -y
htpasswd -c /etc/nginx/.htpasswd.aptly repo-api
chmod o-rw /etc/nginx/.htpasswd.aptly
chown www-data: /etc/nginx/.htpasswd.aptly
```
