+++
date = "2016-03-14"
tags = ["aptly"]
title = "aptly CLI tool"

+++

As the [Aptly API](http://www.aptly.info/doc/api/) is well documented, you can implement your own client using curl/python/ruby/go/whatever or just use [this one](https://github.com/sepulworld/aptly_cli) written in Ruby.

## Installation

```bash
gem install aptly_cli
```

Simply as that. If you want to read further and use your API with [basic authentication](/posts/protect-aptly-api-with-basic-authentication), ~~you can check if my [pull request](https://github.com/sepulworld/aptly_cli/pull/76) has been accepted or just build the gem on your own~~.

### Basic Authentication

---

**UPDATE 2016-03-16**: PR accepted, just pull the official gem

---

> git clone -b basic-auth https://github.com/morph027/aptly_cli.git
>
> cd aptly_git
>
> gem build aptly_cli.gemspec
>
> gem install commander httmultiparty
>
> gem install --local aptly_cli-0.2.1.gem

## Configuration

Further info can be found on the Github page of the project. Just in case, **if** you're using basic auth **and if** the PR isn't merged yet, just add this to the config:

```bash
:username: api-user
:password: api-password
```
	
## Usage

At the moment, the CLI tool is missing to publish a repo using a prefix (which is something you want if you like to host different repos). So we create and publish the repo directly on the aptly host and use the _publish\_update_ function of the CLI tool later to update after adding or removing packages.

```bash
aptly repo create test
aptly publish repo -distribution="trusty" -architectures=""amd64 test :test
```

Here's an example showing how to upload a file to this already published repo:

```bash
wget http://download.morph027.de/apt-find-proxy_1.0.1-2_amd64.deb
aptly-cli file_upload --upload apt-find-proxy_1.0.1-2_amd64.deb --directory /some-upload-dir
```
	
```json
some-upload-dir/apt-find-proxy\_1.0.1-2\_amd64.deb
```

```bash
aptly-cli repo_upload --name test --dir /some-upload-dir --file apt-find-proxy_1.0.1-2_amd64.deb
```

```json
{"FailedFiles"=>[], "Report"=>{"Warnings"=>[], "Added"=>["apt-find-proxy_1.0.1-2_amd64 added"], "Removed"=>[]}}
```

```bash
aptly-cli publish_update --prefix test --distribution trusty
```

```json
{"Architectures"=>["amd64"], "Distribution"=>"trusty", "Label"=>"", "Origin"=>"", "Prefix"=>"test", "SourceKind"=>"local", "Sources"=>[{"Component"=>"main", "Name"=>"test"}], "Storage"=>""}
```
