+++
title = "Running Collabora Online Development Edition without Docker"
description = ""
author = "morph027"
tags = ["nextcloud", "collabora", "docker"]
date = "2018-02-09T19:00:00+02:00"
+++

Besides my usual intention to run everything with Docker, this time i wanted to run [Collabora Online Development Edition (CODE)](https://www.collaboraoffice.com/code/) without Docker (on a VPS which is unable to run Docker).

The link above provides instructions on how to setup repositories and packages for your distribution. To be able to start the service, you'll just need to create some self-signed certificates (and run the setup behind nginx or Apache reverse proxy).

Otherwise `loolwsd` will fail with errors like:

```
loolwsd[21181]: wsd-21181-21181 15:41:45.188601 [ loolwsd ] FTL  Cannot load CA file/directory at /etc/loolwsd/ca-chain.cert.pem (error:02001002:system library:fopen:No such file or directory)| wsd/LOOLWSD.cpp:2876
loolwsd[21181]: Cannot load CA file/directory at /etc/loolwsd/ca-chain.cert.pem (error:02001002:system library:fopen:No such file or directory)
```

## Create certificates

I just grabbed the steps to create the certificates from inside the docker image (at `start-libreoffice.sh`):

```bash
openssl genrsa -out /etc/loolwsd/root.key.pem 2048
openssl req -x509 -new -nodes -key /etc/loolwsd/root.key.pem -days 9131 -out /etc/loolwsd/ca-chain.cert.pem -subj "/C=DE/ST=BW/L=Stuttgart/O=Dummy Authority/CN=Dummy Authority"
openssl genrsa -out /etc/loolwsd/key.pem 2048 -key /etc/loolwsd/key.pem
openssl req -key /etc/loolwsd/key.pem -new -sha256 -out /etc/loolwsd/localhost.csr.pem -subj "/C=DE/ST=BW/L=Stuttgart/O=Dummy Authority/CN=localhost"
openssl x509 -req -in /etc/loolwsd/localhost.csr.pem -CA /etc/loolwsd/ca-chain.cert.pem -CAkey /etc/loolwsd/root.key.pem -CAcreateserial -out /etc/loolwsd/cert.pem -days 9131
```

Some additional security settings:

```bash
chgrp lool /etc/loolwsd/key.pem
chmod g+r /etc/loolwsd/key.pem
```

## Setup WOPI

Finally adjust your trusted domain in `/etc/loolwsd/loolwsd.xml` (at line 88, replace `localhost` with the domain with the WOPI client you're running like Nextcloud):

```xml
    <storage desc="Backend storage">
        <filesystem allow="false" />
        <wopi desc="Allow/deny wopi storage. Mutually exclusive with webdav." allow="true">
            <host desc="Regex pattern of hostname to allow or deny." allow="true">yourcloud.yourhost.com</host>
            <host desc="Regex pattern of hostname to allow or deny." allow="true">10\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}</host>
            <host desc="Regex pattern of hostname to allow or deny." allow="true">172\.1[6789]\.[0-9]{1,3}\.[0-9]{1,3}</host>
            <host desc="Regex pattern of hostname to allow or deny." allow="true">172\.2[0-9]\.[0-9]{1,3}\.[0-9]{1,3}</host>
            <host desc="Regex pattern of hostname to allow or deny." allow="true">172\.3[01]\.[0-9]{1,3}\.[0-9]{1,3}</host>
            <host desc="Regex pattern of hostname to allow or deny." allow="true">192\.168\.[0-9]{1,3}\.[0-9]{1,3}</host>
            <host desc="Regex pattern of hostname to allow or deny." allow="false">192\.168\.1\.1</host>
            <max_file_size desc="Maximum document size in bytes to load. 0 for unlimited." type="uint">0</max_file_size>
        </wopi>
        <webdav desc="Allow/deny webdav storage. Mutually exclusive with wopi." allow="false">
            <host desc="Hostname to allow" allow="false">localhost</host>
        </webdav>
    </storage>
```
