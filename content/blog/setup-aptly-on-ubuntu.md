+++
date = "2016-03-13"
tags = ["aptly"]
title = "Setup aptly on Ubuntu"

+++

> aptly is a swiss army knife for Debian repository management: it allows you to mirror remote repositories, manage local package repositories, take snapshots, pull new versions of packages along with dependencies, publish as Debian repository. You can try it right now just for free.
> 
> aptly is available both as CLI tool and HTTP REST service.

Nuff said, we want this ☺

## Installation

Aptly provides a [repo](http://www.aptly.info/download/) (with an outdated key, do not wonder) to install the single binary (written in Go).

## Preparation

### aptly

First, we create a dedicated aptly user (real user, as we want to use it later for CLI actions):

```useradd -m -s /bin/bash -G sudo aptly```

Now lets create and publish a repo, causing aptly to initialize a few things:

```aptly repo create ubuntu```

```aptly publish repo -distribution="trusty" ubuntu :ubuntu```

_:ubuntu_ is a just prefix, more info [here](http://www.aptly.info/doc/aptly/publish/repo/).

### GPG

For signing everything, we would also need a GPG key. You can use one with a passphrase and pass the passphrase with the API later or just use one without a passphrase. If you do it another machine (e.g. not the remote server), you'll need to export/import the pair.

* [Creating GPG Keys](https://fedoraproject.org/wiki/Creating_GPG_Keys#Creating_GPG_Keys_Using_the_Command_Line)
* [Exporting GPG Keys](https://fedoraproject.org/wiki/Creating_GPG_Keys#Making_a_Key_Backup_Using_the_Command_Line)

Import can be done like this:

```gpg --import /tmp/aptly-secret-key.asc```

Now we should have some files and directories in our aptly home folder:

```
.
|-- .aptly
|   |-- db
|   |   |-- 000002.ldb
|   |   |-- 000005.ldb
|   |   |-- 000018.log
|   |   |-- CURRENT
|   |   |-- LOCK
|   |   |-- LOG
|   |   |-- LOG.old
|   |   `-- MANIFEST-000019
|   `-- public
|       |-- ubuntu
|           |-- dists
|           |   `-- trusty
|           `-- pool
|-- .aptly.conf
`-- .gnupg
    |-- gpg.conf
    |-- pubring.gpg
    |-- pubring.gpg~
    |-- secring.gpg
    `-- trustdb.gpg
```

### Upstart

To start the API server automatically, here's an upstart script (_/etc/init/aptly-api.conf_) doing things fine:

```
description "aptly-api"
start on filesystem or runlevel [2345]
stop on runlevel [!2345]
respawn
umask 022
chdir /home/aptly
setuid aptly
setgid aptly
console log # log stdout/stderr to /var/log/upstart/aptly-api.log
exec /usr/bin/aptly api serve -no-lock
```
	
Create the run-dir and the log file:

```touch /var/log/upstart/aptly-api.log```

```chown aptly: var/log/upstart/aptly-api.log```

```mkdir /var/run/aptly && chown aptly: /var/run/aptly```

## Start & Test

If everything went fine, we can now issue an ```service aptly-api start``` and test the API:

```curl localhost:8080/api/repos```

should result in some JSON output like

```json
[{"Name":"ubuntu","Comment":"","DefaultDistribution":"","DefaultComponent":"main"}]
```

Now we are ready to fill and publish our repositories! Also think about [protecting](/posts/protect-aptly-api-with-basic-authentication) the API access! And check the [API CLI tool](/posts/aptly-cli-tool)...
