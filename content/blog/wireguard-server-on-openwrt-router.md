+++
title = "Setting up a wireguard server running on an OpenWRT router"
date = "2018-11-29T08:00:50+02:00"
tags = ["openwrt", "wireguard"]
author = "morph027 feat. casept"
+++

The blog post from [casept](https://casept.github.io/post/wireguard-server-on-openwrt-router/) was almost working for me, except for one little thing to mention in section _You'll also need to set the client up on your server_

`uci add_list network.@wireguard_wg0[-1].allowed_ips="0.0.0.0/0"` (obviously) is replacing the existing default route, which renders internet unusable ;)

As we're routing the clients traffic anyway, we can just allow the single host address like `uci add_list network.@wireguard_wg0[-1].allowed_ips="192.168.199.2/32"` (192.168.199.0/24 as WireGuards subnet in this example). 
