+++
date = "2015-04-18"
tags = ["check_mk"]
title = "send check_mk notifications using TextSecure"

+++

Just created this small helpful [plugin](https://github.com/morph027/check_mk-notification-textsecure) for check\_mk which allows you to send event notifications via TextSecure.
