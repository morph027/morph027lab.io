+++
author = ""
date = "2016-11-11T15:06:24+01:00"
description = ""
tags = ["cinnamon"]
title = "Cinnamon 3.2 released, how to install on Ubuntu"

+++

Cinnamon 3.2.1 was [released](https://github.com/linuxmint/Cinnamon/releases) recently and i've glued some stuff from the internet to [create packages](https://gitlab.com/packaging/cinnamon) for Ubuntu.

---

**UPDATE 2016-11-20**: [Cinnamon Stable PPA](https://launchpad.net/~embrosyn/+archive/ubuntu/cinnamon) by embrosyn is up to date now!

---
