+++
date = "2016-03-15"
tags = ["aptly"]
title = "Push DEB packages from CI jobs to aptly repo"

+++

Now that we got aptly up and running, we can use it to upload packages. Practical example would be building packages in some CI (Jenkins, Gitlab, Travis, Drone, you-name-it, ...), probably add some tests and then upload the package to a repo.

Example can be found [here](https://gitlab.com/packaging/drone) in file ```upload-package.sh```. Take notice of some variables being sourced inside ```.gitlab-ci.yml``` before (_$PACKAGE\_FILE_, _$ARCH_, _$DIST_).
