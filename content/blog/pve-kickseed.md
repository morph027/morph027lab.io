+++
date = "2016-09-23T12:46:50+02:00"
title = "PVE: Kickstart CentOS/Preseed Ubuntu"
tags = ["proxmox", "centos", "ubuntu", "preseed", "kickstart"]

+++

You can use KVMs direct kernel boot feature to boot things without having an ISO or using PXE. These options can be set in ```args:``` in Proxmox. Doing so, we can simply kickstart a CentOS or preseed an Ubuntu installation by passing all necessary options.

---
**UPDATE 2017-11-06**

* [Ansible approach](https://gitlab.com/morph027/pve-infra-poc)

---

## Example:

### CentOS

#### Prepare kernel

Grab CentOS kernel (if not alreay done, then skip this step and adjust paths in ```qm create``` command)

```bash
wget -O /tmp/vmlinuz http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/isolinux/vmlinuz
wget -O /tmp/initrd.img http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/isolinux/initrd.img

```

#### Create VM

Your milage may vary, please set up according to your cluster and needs (CPU, Memory, Network, ...)!

> <i class="fa fa-info-circle" aria-hidden="true"></i> Make sure the ```args``` are on one single line, even if it is ugly to read

```bash
export VM=$(pvesh get /cluster/nextid | grep -oE '[0-9]*')
qm create $VM \
--virtio0 my-pve-vm-storage:32 \
--net0 virtio,bridge=vmbr0 \
--bootdisk virtio0 \
--ostype l26 \
--memory 1024 \
--balloon 4096 \
--cores 4 \
--args '-kernel /tmp/vmlinuz -initrd /tmp/initrd.img -append "inst.stage2=http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/ inst.repo=http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/ inst.ks=https://gitlab.com/morph027/kickstart-gitlab-ci-multi-runner/raw/master/gitlab-runner.cfg rd.noverifyssl"'
```

### Ubuntu

#### Prepare Kernel

Grab Ubuntu kernel (if not alreay done, then skip this step and adjust paths in ```qm create``` command)

```bash
cd /tmp
wget http://archive.ubuntu.com/ubuntu/dists/xenial/main/installer-amd64/current/images/netboot/netboot.tar.gz
tar xzf netboot.tar.gz 
mv ubuntu-installer/amd64/linux ubuntu-installer/amd64/initrd.gz .
rm -rf ubuntu-installer netboot.tar.gz ldlinux.c32 pxelinux.0 pxelinux.cfg version.info
```

#### Create VM

Your milage may vary, please set up according to your cluster and needs (CPU, Memory, Network, ...)!

> <i class="fa fa-info-circle" aria-hidden="true"></i> Make sure the ```args``` are on one single line, even if it is ugly to read

```bash
export VM=$(pvesh get /cluster/nextid | grep -oE '[0-9]*')
qm create $VM \
--virtio0 my-pve-vm-storage:32 \
--net0 virtio,bridge=vmbr0 \
--bootdisk virtio0 \
--ostype l26 \
--memory 1024 \
--balloon 4096 \
--cores 4 \
--args '-kernel /tmp/linux -initrd /tmp/initrd.gz -append "preseed/url=https://gitlab.com/morph027/preseed-gitlab-ci-multi-runner/raw/master/gitlab-ci-multi-runner.seed debian-installer/allow_unauthenticated_ssl=true locale=de_DE.UTF-8 debian/priority=critical vga=normal debian-installer/keymap=de console-keymaps-at/keymap=de console-setup/layoutcode=de_DE netcfg/choose_interface=auto localechooser/translation/warn-light=true localechooser/translation/warn-severe=true console-setup/ask_detect=false netcfg/get_hostname=PRESEED FRONTEND_BACKGROUND=original"'
```

### Kickstart/Preseed VM

- start and install
- watch status and delete ```args:```, when installation is finished (You need to poweroff the virtual machine or the operation is just pending and your vm will end up in an endless installation cycle)
- start vm again to boot into new system

```bash
qm start $VM \
&& ((while (qm status $VM | grep -o running); do sleep 10; done) && qm set $VM -delete args) \
&& qm start $VM
```

If you need to have ```args:```, then replace

```
qm set $VM -delete args
```

with

```
pvesh set /nodes/$HOSTNAME/qemu/$VM/config --args "-no-reboot -no-hpet"
```
