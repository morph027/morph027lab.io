+++
title = "Setting up wireless roaming using 802.11r fast transition on OpenWRT"
date = "2019-02-03T08:00:50+02:00"
tags = ["openwrt"]
author = "morph027"
+++

Ages ago, i was setting up 802.11r the [complicated way](https://www.reddit.com/r/openwrt/comments/515oea/finally_got_80211r_roaming_working/) using all the `r0kh` and `r1kh` lists across all access points, which worked well but was a little pita to setup.

After creating a little [helper](https://gitlab.com/morph027/openwrt-uci-helper-802.11r-fast-transition), i just discovered that recent OpenWRT builds can cope with the option `ft_psk_generate_local` to dynamically create the RADIUS keys and stuff. Even with Luci support, just enable `802.11r Fast Transition` and `Generate PMK locally`, set a `Mobility Domain` (same across all AP's per SSID) and a `NAS ID` (unique, i'd still prefer the BSSID w/o the colons) and it works out of the box.

Just for the sake of having some code in a blog post, here's the command line way:

Either in UCI (where `x` is the wifi interface of your SSID):

```bash
uci set wireless.@wifi-iface[x].ieee80211r='1'
uci set wireless.@wifi-iface[x].mobility_domain='67c5'
uci set wireless.@wifi-iface[x].ft_psk_generate_local='1'
uci set wireless.@wifi-iface[x].nasid='112233445500'
uci commit wireless
wifi reload
```

Or in the config files:

```bash
	option ieee80211r '1'
	option nasid '112233445500'
	option mobility_domain '67c5'
	option ft_psk_generate_local '1'
```

In the end you should end up roaming around your area and see log entries like

```bash
Sun Feb  3 15:49:44 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session 3AD81FF09336209D
Sun Feb  3 15:50:54 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session A24BD89120231531
Sun Feb  3 15:57:01 2019 daemon.info hostapd: wlan0: STA <client-mac-address> RADIUS: starting accounting session 0F16B12ED50B4144
Sun Feb  3 16:21:06 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session DADA831386E648BB
Sun Feb  3 16:36:12 2019 daemon.info hostapd: wlan0: STA <client-mac-address> RADIUS: starting accounting session 11C6B337CF924447
Sun Feb  3 18:12:09 2019 daemon.info hostapd: wlan0-1: STA  RADIUS: starting accounting session A84B0B52CFAC0849
Sun Feb  3 18:28:04 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session BE0A9D794BF13003
Sun Feb  3 18:34:44 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session FC23A4D99EEC830B
Sun Feb  3 19:37:19 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session FA1548B76C0A2FB6
Sun Feb  3 19:37:19 2019 daemon.info hostapd: wlan0-1: STA c4:36:6c:3b:2f:fc RADIUS: starting accounting session C8B43DA60E78A830
Sun Feb  3 19:37:21 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session 82B42AC01AFFD40D
Sun Feb  3 19:37:24 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session 60FCF632B3D503CE
Sun Feb  3 19:37:26 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session EEBA1E796816A983
Sun Feb  3 19:37:29 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session A1806FDA389EC37A
Sun Feb  3 19:37:31 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session 04794B3C6ED1DC9A
Sun Feb  3 19:37:34 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session BEDF86A2A1E2C983
Sun Feb  3 19:37:36 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session 4C2822506767F420
Sun Feb  3 19:37:39 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session 02B9C390FD864A25
Sun Feb  3 19:37:42 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session C4A203F243F9A02E
Sun Feb  3 19:37:44 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session BD38335454CCF872
Sun Feb  3 19:37:46 2019 daemon.info hostapd: wlan0-1: STA <client-mac-address> RADIUS: starting accounting session AC3BC59922195B16
Sun Feb  3 19:38:52 2019 daemon.info hostapd: wlan1: STA <client-mac-address> RADIUS: starting accounting session 24BC61A554C47B6E
...
```
