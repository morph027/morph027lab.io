+++
date = "2016-03-12"
tags = ["go"]
title = "Lightweight Git Service and CI"

+++

In cases, where keeping code private (as in never never let it leave your storage/network) and you don't want to host a full-blown [Gitlab](https://about.gitlab.com/) instance, just have a look at [Gogs](https://gogs.io/) and [Drone](https://github.com/drone/drone). Both projects are written in [Go](https://golang.org/), which runs very fast even on low performance systems (i.e. a RPi or something similar).
