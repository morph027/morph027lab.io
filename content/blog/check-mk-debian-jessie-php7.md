+++
date = "2016-10-17T08:00:05+02:00"
tags = ['php', 'check_mk']
title = "Check_MK / Debian Jessie  with php7"

+++

If you want to run Check\_MK with PHP7.0 (Dotdeb) on Debian Jessie, you need to update the package dependencies.

Make sure you run these commands as root, or file permissions will be wrong after repacking.

```bash
wget https://mathias-kettner.de/support/1.2.8p12/check-mk-raw-1.2.8p12_0.jessie_amd64.deb
mkdir check-mk-raw-1.2.8p12_0_php7.0.jessie_amd64
dpkg-deb -R check-mk-raw-1.2.8p12_0.jessie_amd64.deb check-mk-raw-1.2.8p12_0_php7.0.jessie_amd64
cd check-mk-raw-1.2.8p12_0_php7.0.jessie_amd64
sed -i 's,php5,php7.0,g' DEBIAN/control
sed -i 's,php7.0-sqlite,php7.0-sqlite3,g' DEBIAN/control
for file in $(grep -sRli php5-cgi opt/omd/versions/1.2.8p12.cre/skel)
do
  sed -i 's,/usr/bin/php5-cgi,/usr/lib/cgi-bin/php7.0,g' $file  
done
dpkg-deb -b check-mk-raw-1.2.8p12_0_php7.0.jessie_amd64 check-mk-raw-1.2.8p12_0_php7.0.jessie_amd64.deb
```

Also you need to adjust existing site configs to use the new php binaries:

```bash
cd /omd/sites/$SITE_NAME
for file in $(grep -sRli php5-cgi etc)
do
  sed -i 's,/usr/bin/php5-cgi,/usr/lib/cgi-bin/php7.0,g' $file
done
```
