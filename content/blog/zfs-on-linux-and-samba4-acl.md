+++
date = "2015-10-16"
tags = ["zfs"]
title = "ZFS on Linux and SAMBA4 ACL"

+++

Recently, i was trying to setup a SAMBA4 domain controller inside a LXC VM on Proxmox using ZFS. Kind a lot of fancy buzzwords and acronyms ;)

However, domain provision failed due to missing *acl* flag on filesystem:

```
Realm [LAN]: LAN.EXAMPLE.COM
 Domain [LAN]: 
 Server Role (dc, member, standalone) [dc]: 
 DNS backend (SAMBA_INTERNAL, BIND9_FLATFILE, BIND9_DLZ, NONE) [SAMBA_INTERNAL]: 
 DNS forwarder IP address (write 'none' to disable forwarding) [1.2.3.4]: 
Administrator password: 
Retype password: 
Looking up IPv4 addresses
Looking up IPv6 addresses
No IPv6 address will be assigned
Setting up share.ldb
Setting up secrets.ldb
Setting up the registry
Setting up the privileges database
Setting up idmap db
Setting up SAM db
Setting up sam.ldb partitions and settings
Setting up sam.ldb rootDSE
Pre-loading the Samba 4 and AD schema
Adding DomainDN: DC=lan,DC=example,DC=com
Adding configuration container
Setting up sam.ldb schema
Setting up sam.ldb configuration data

Setting up display specifiers
Modifying display specifiers
Adding users container
Modifying users container
Adding computers container
Modifying computers container
Setting up sam.ldb data
Setting up well known security principals
Setting up sam.ldb users and groups
Setting up self join
ERROR(<class 'samba.provision.ProvisioningError'>): Provision failed - ProvisioningError: Your filesystem or build does not support posix ACLs, which s3fs requires.  Try the mounting the filesystem with the 'acl' option.
  File "/usr/local/samba/lib/python2.7/site-packages/samba/netcmd/domain.py", line 442, in run
nosync=ldap_backend_nosync, ldap_dryrun_mode=ldap_dryrun_mode)
  File "/usr/local/samba/lib/python2.7/site-packages/samba/provision/__init__.py", line 2172, in provision
skip_sysvolacl=skip_sysvolacl)
  File "/usr/local/samba/lib/python2.7/site-packages/samba/provision/__init__.py", line 1806, in provision_fill
names.domaindn, lp, use_ntvfs)
  File "/usr/local/samba/lib/python2.7/site-packages/samba/provision/__init__.py", line 1558, in setsysvolacl
raise ProvisioningError("Your filesystem or build does not support posix ACLs, which s3fs requires.  "
```
	
Seems legit:

```
root@samba:~# mount | grep ' / '
vms/subvol-107-disk-1 on / type zfs (rw,noatime,xattr,noacl)
```

Found the solutions [here](https://mywushublog.com/2012/05/zfs-and-acls-with-samba/) and [here](http://ubuntuforums.org/showthread.php?t=2214918&p=13246038#post13246038).

Fixing it:

```
root@proxmox:~# zfs set acltype=posixacl vms/subvol-107-disk-1
root@proxmox:~# zfs set aclinherit=passthrough vms/subvol-107-disk-1
```

Et voilà:

```
root@samba:~# mount | grep ' / '
vm-400g/subvol-107-disk-1 on / type zfs (rw,noatime,xattr,posixacl)
```

Of course, you need to do this for every zfs dataset you want to use with samba, e.g. any [bind mount](https://pve.proxmox.com/wiki/LXC_Bind_Mounts) for lxc inside proxmox.
