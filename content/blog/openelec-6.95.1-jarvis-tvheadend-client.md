+++
date = "2016-03-25T12:35:08+01:00"
tags = ["kodi","openelec","cubox"]
title = "Cubox-i/RPi2 OpenELEC 6.95.1 Beta (Kodi 16 Jarvis) Tvheadend client"

+++

I've just installed the first beta of OpenELEC 7 a.k.a. 6.95.1 with Kodi v16 Jarvis on my Cubox-i and RPi2. The Tvheadend service was updated and worked straight out of the box, unfortunately the client does not due to mismatching API versions. Following the steps outlined in [this thread](http://forum.solid-run.com/kodi-xbmc-player-on-cubox-i-and-hummingboard-f7/-devel-openelec-master-kodi-16-0-jarvis-alpha-t2694.html), i managed to compile an updated version:

## docker build

```
mkdir -p ~/openelec/builds
docker run -it --name imx6 -v ~/openelec/builds/:/builds damonmorgan/openelec-dependencies /bin/bash
cd /builds/
git clone -b 6.95.1 https://github.com/OpenELEC/OpenELEC.tv.git
cd OpenELEC.tv
PROJECT=imx6 ARCH=arm ./scripts/create_addon pvr.hts
```

Go and grab a huge can of <i class="fa fa-coffee"></i> as this is going to take a loooooooooooong while:

```
# @Intel(R) Xeon(R) CPU E3-1240 v5 @ 3.50GHz / 64GB RAM / SSD

...
...
...
  CREATE ADDON  (imx6/arm) pvr.hts
*** compressing Addon pvr.hts ... ***

real    97m49.712s
user    141m28.648s
sys     16m6.556s

```

Just in case you don't want to wait that almost 1.5 hours (on a really capable machine), i'll provide my zip for you guys: [pvr.hts-2.2.14-armv7.zip](/pvr.hts-2.2.14-armv7.zip). It works (tested here) for all _ARMv7_ cpus, including Cubox-i (_Freescale i.MX6_) and RPi2. 
