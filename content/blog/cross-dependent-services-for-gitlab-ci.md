+++
title = "Cross linked services for Gitlab CI"
description = "Using Docker Swarm"
author = "morph027"
tags = ["docker", "gitlab"]
date = "2017-11-19T13:00:00+02:00"
+++

Gitlab CI does a great job when [used with Docker](https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#what-is-a-service) and dependencies, e.g. your app tests need a database or a webserver or whatsoever. Unfortunateley, this does not involve complex setups where you need multiple services connected to each other like Selenium Grid and it's nodes or probably an API and it's database.

Let's asssume you have your fancy app, which talks to an API, which itselfs needs a database to run (and you probably want to test against the same database like in production and not sqlite).

![](/img/gitlab-ci-swarm.png)

This will not work with Gitlab, cause you can spin up your API and your database, but they are just not connected.

![](/img/gitlab-ci-swarm-not-working.png)

## Docker Swarm to the rescue!

So hey, just let Swarm run these things and then test against it. I'm not showing this, you probably know your requirements much better than me and also there is excellent [Swarm Documentation](https://docs.docker.com/engine/swarm/). For deploying to Swarm, just use the stock tools like shown in [the other blog post](/post/gitlab-ci-with-docker-swarm/). Now you've got it working:

![](/img/gitlab-ci-swarm-working.png)

## Best practices

* you can expose the ports you're accessing during test (take care of conflicting apps)
* or much better use some sort of a load balancer like [Traefik](https://traefik.io/) or [Docker Flow Proxy](http://proxy.dockerflow.com/)
* add some "await" feature to your services in case dependencies are not up yet (either on code or startup script, i use [await](https://github.com/betalo-sweden/await))

If you need further advice or tips, just drop a comment below or on twitter...
