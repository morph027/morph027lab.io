+++
date = "2016-09-06T09:00:00+01:00"
tags = ["plymouth", "lightdm", "ubuntu"]
title = "Branded Ubuntu with Cinnamon Desktop"

+++

Utilizing some projects, you can achive a nice branded CI/CD Ubuntu:

* [preseed-cinnamon-ubuntu](https://gitlab.com/morph027/preseed-cinnamon-ubuntu)
* [plymouth-logo-theme-template](https://gitlab.com/packaging/plymouth-logo-theme-template)
* [lightdm-logo-theme-template](https://gitlab.com/packaging/lightdm-logo-theme-template)

{{< vimeo 181592540 >}}
