+++
date = "2016-03-25T17:34:35+01:00"
tags = ["jabber", "pidgin"]
title = "Pidgin with XEP-0280 Message Carbons"

+++

If you have multiple clients connected to a jabber server with XEP-0280 enabled, you may want to receive all messages on all clients. ATM, Pidgin does not support this, but there's a patch available. I'm just picking up this [comment](https://developer.pidgin.im/ticket/15508#comment:22) on how to build it:

```
mkdir -p ~/pidgin
docker run --rm -ti -v ~/pidgin:/pidgin -w /pidgin ubuntu:xenial /bin/bash
apt-get update
apt-get install curl dpkg-dev -y
apt-get source pidgin
apt-get -y build-dep pidgin
cd pidgin-*
curl -O https://developer.pidgin.im/raw-attachment/ticket/15508/carbons.5.patch
patch -p1 < carbons.5.patch
dpkg-source --commit
dpkg-buildpackage -us -uc
```

Just adjust the docker image tag to the ubuntu version you use (i always use LTS <i class="fa fa-smile-o"></i>). You can find prebuilt packages [here](https://repo.morph027.de/ubuntu/pool/main/p/pidgin/) in my [repo](https://repo.morph027.de/readme.txt).
