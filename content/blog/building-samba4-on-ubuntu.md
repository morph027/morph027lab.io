+++
date = "2013-07-01"
title = "Building SAMBA4 on Ubuntu"
tags = ["samba4"]

+++

Actually, reading [the official docs](https://wiki.samba.org/index.php/Building_samba_4_from_source) should be fine. However, here are some experiences and hints.

## Prerequisites

For successfully building SAMBA4, you need some extra packages on your Ubuntu machine/vm:

```
apt-get install build-essential libattr1-dev libblkid-dev libgnutls-dev libreadline-dev python-dev autoconf python-dnspython gdb pkg-config bind9utils resolvconf krb5-user git-core screen acl python-dev libgcrypt11-dev libtalloc-dev libldb-dev libtevent-dev zlib1g-dev libacl1-dev libattr1-dev libblkid-dev libgnutls-dev libreadline-dev python-dev python-dnspython gdb pkg-config libpopt-dev libldap2-dev dnsutils
```

## Source code

```
git clone git://git.samba.org/samba.git samba-master
cd samba-master
git checkout v4-0-stable
```

## Building

We are just using the included Makefile. As we want to use LDAP with Start TLS later, we need to pass the appropriate parameter before actually compiling.

```
./configure --enable-gnutls
make
```

**Note:** If you'r already running a samba instance, you need to stop it before installing:

```
service samba4 stop
```

Making backups is always a good idea, assuming you installed into the default directory:

```
cp -R /usr/local/samba /usr/local/samba.bak
```

Finally, install the files:

```
make install
```

## Ubuntu Upstart

Just paste the [following](https://wiki.samba.org/index.php/Samba4/InitScript) into */etc/init/samba4.conf*:

```
description "SMB/CIFS File and Active Directory Server"
author      "Jelmer Vernooij <jelmer@ubuntu.com>"
start on (local-filesystems and net-device-up)
stop on runlevel [!2345]
expect fork
normal exit 0
pre-start script
	[ -r /etc/default/samba4 ] && . /etc/default/samba4
	install -o root -g root -m 755 -d /var/run/samba
	install -o root -g root -m 755 -d /var/log/samba
end script
exec /usr/local/samba/sbin/samba -D
```
