+++
author = "morph027"
date = "2017-07-13T08:05:00+02:00"
description = ""
tags = ["proxmox"]
title = "Fast reboots for Proxmox with kexec"

+++

If you want to use kexec for faster (warm) reboots with Proxmox, have a look at my [forum post](https://forum.proxmox.com/threads/tip-fast-reboots-with-kexec.35624/).
