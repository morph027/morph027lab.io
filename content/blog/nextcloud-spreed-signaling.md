+++
title = "Setup nextcloud-spreed-signaling standalone server on Ubuntu"
description = ""
author = "morph027"
tags = ["nextcloud"]
date = "2020-05-19T20:00:00+02:00"
+++

Thanks to [struktur AG](https://www.struktur.de/), which released the open source version of the standalone signaling server for Nextcloud Talk, you can now run your own version of the Talk High Performance Backend. Awesome work and good move!

I've grabbed the sources immediately and built a HPB on one of my tiny VPS (VPS 200 G8 hosted by [netcup](https://www.netcup.de)).

**Notice:** Your setup might differ a lot (or won't apply at all as your're using different OS, Webserver, ...). This is just a writeup of steps i took to make it work.

There's also a [pull request](https://github.com/strukturag/nextcloud-spreed-signaling/pull/7) containing a dockerized setup.

## Requirements

* a Ubuntu server (despite the software should be available for other systems too, i'll only cover Ubuntu)
* Nextcloud with Talk installed and enabled (not necessarily the machine you want to run HPB on, my NC server is private while the signaling server is public)
* a server with a public network interface (port forwarding works too)

## Installation

### Janus WebRTC Server

* install the `janus` package from the repo (up to date backports for Ubuntu Bionic/Focal can be found [here](https://gitlab.com/packaging/janus/)):
  * `sudo apt install janus`
* create a random api key for turn server usage: `openssl rand -base64 16` (will be used in nextcloud-spreed-signaling later)
* edit `/etc/janus/janus.jcfg` (`.cfg` in older versions), search for `turn_rest_api_key` and insert the generated key
* search for the line `full_trickle = true` and uncomment
* edit `/etc/janus/janus.transport.http.jcfg` and set `interface = "lo"` in the `general` section
* edit `/etc/janus/janus.transport.websockets.jcfg` and set `interface = "lo"` in the `general` section
* restart janus service: `systemctl restart janus` (do not forget to enable on boot: `systemctl enable janus`)

Janus is listening on localhost interface and will be hidden by the HBP.

### Coturn TURN Server

Follow [Nextcloud HowTo](https://help.nextcloud.com/t/howto-setup-nextcloud-talk-with-turn-server/30794).

### NATS server

The NATS messaging server can be installed in [various ways](https://docs.nats.io/nats-server/installation). I've also created apt packages [here](https://gitlab.com/packaging/nats-server) which i'm using.

### nextcloud-spreed-signaling server (HPB)

Either build the binary yourself ([see here](https://github.com/strukturag/nextcloud-spreed-signaling/issues/4#issuecomment-630982439)) or use my [prebuilt ubuntu packages](https://gitlab.com/packaging/nextcloud-spreed-signaling).

* create `/etc/signaling/server.conf` and copy content from the [example](https://github.com/strukturag/nextcloud-spreed-signaling/blob/master/server.conf.in)
* create 2 random hex keys:
  * `openssl rand -hex 16`
* adjust config file:
  * uncomment the http listener
  * add first random hex key to parameter `hashkey` below `[sessions]`
  * add second random hex key to parameter `blockkey` below `[sessions]`
  * add the apikey from janus first step to `[turn]` section's `apikey` parameter
  * add the turn server (e.g. `servers = turn:your.public.fqdn:3478?transport=tcp`)
  * add `url = ws://127.0.0.1:8188` to `[mcu]` sections `url` parameter

Start the standalone signaling server: `systemctl restart signaling`.

It is advised to listen on localhost and put a [reverse proxy](https://github.com/strukturag/nextcloud-spreed-signaling/#setup-of-frontend-webserver) in front of the gateway. I'm using [Caddy](https://caddyserver.com/), see next section.

### Caddy Reverse Proxy

* grab latest debian package from [Caddy releases](https://github.com/caddyserver/caddy/releases)
* install caddy package `apt-get -y install ./caddy_2.0.0_linux_<arch>.deb`
* edit `/etc/caddy/Caddyfile` and insert sections for janus and signaling server:

```plain
your.public.fqdn {
  log
  route /standalone-signaling/* {
    uri strip_prefix /standalone-signaling
    reverse_proxy http://127.0.0.1:8081
  }
}
```

---
### Updates

* 2020-06-06: revert janus to localhost without reverse proxy
* 2020-05-26: added NATS server section
