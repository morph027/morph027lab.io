+++
date = "2016-09-21T20:13:11+02:00"
tags = ["nextcloud"]
title = "Nextcloud install via packages on Debian Jessie"

+++

Similiar to the "old" ```owncloud-files``` package, which allows to have an up to date version of it while still beiing able to maintain your custom setup around it (webserver stack, database server, ...), i've created a package for [Nextcloud](https://gitlab.com/packaging/nextcloud).

It get's uploaded to my [repo](ihttps://repo.morph027.de/), so can throw this in a mix with MariaDB and Dotdeb PHP7.0 repos to easily create your Nextcloud instance.
