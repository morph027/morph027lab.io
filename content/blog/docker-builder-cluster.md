+++
title = "Clustered multiarchitecture docker buildx nodes"
date = "2024-12-24T09:00:00+02:00"
tags = ["development"]
author = "morph027"
+++

When building multiarchitecture container images, the most common setup is using QEMU. While this works quite well, it also performs quite slow due to the required virtualization layer involved.

Most builds today are targeting `linux/amd64` and `linux/arm64` thanks to the availability of client and server hardware getting better (MacBooks, Ampere, Graviton, ...) and of course the Raspberry Pi 😀.

If you have access to multiple nodes using different architecture but want to create a single multiarch image without stitching 2 independent images together (`docker buildx imagetools create`), you can use a cluster of multiple nodes to do so.

## Setup

1. Setup [secure remote access](https://docs.docker.com/engine/security/protect-access/#use-tls-https-to-protect-the-docker-daemon-socket) and expose[^1] the docker daemon on the target node
1. Create a new context using the certs from previous step:
    ```bash
    docker context create remote --docker "host=tcp://${DOCKER_REMOTE}:2376,ca=${DOCKER_CA},cert=${DOCKER_CERT},key=${DOCKER_KEY}"
    ```
1. Create a new build instance on the local machine:
    ```bash
    docker buildx create --name cluster --node local --platform linux/amd64
    ```
1. Attach the remote node to the local builder using it's context
    ```bash
    docker buildx create --name cluster --node remote --use remote --append
    ```
1. To use the clustered builder:
    ```bash
    docker buildx use cluster
    ```
1. Prepare the build nodes w/ buildkit:
    ```bash
    docker buildx inspect --bootstrap
    ```
1. Check setup:
    ```bash
    docker buildx ls
    ```
    Resulting output should look like this:
    ```bash
    NAME/NODE        DRIVER/ENDPOINT                   STATUS    BUILDKIT   PLATFORMS
    cluster*         docker-container                                       
     \_ local         \_ unix:///var/run/docker.sock   running   v0.18.2    linux/amd64* (+3)
     \_ remote        \_ remote                        running   v0.18.2    linux/arm64, linux/arm (+2)
    default          docker                                                 
     \_ default       \_ default                       running   v0.17.3    linux/amd64 (+3)
    remote           docker                                                 
     \_ remote        \_ remote                        running   v0.17.3    linux/arm64, linux/arm (+2)
    ```
1. Build the image (example):
    ```bash
    docker buildx build \
    --provenance=false \
    --platform linux/amd64,linux/arm64 \
    --push \
    -t your-multiarch-image
    .
    ```

[^1]: Add systemd dropin:
    ```bash
    mkdir -p /etc/systemd/system/docker.service.d/
    cat > /etc/systemd/system/docker.service.d/00-tls.conf<<EOF
    [Service]
    ExecStart=
    ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --tlsverify --tlscacert=/etc/docker/ca.pem --tlscert=/etc/docker/server-cert.pem --tlskey=/etc/docker/server-key.pem -H=0.0.0.0:2376
    EOF
    ```
