+++
title = "Swift 3 with Ryzen 3500u, WD SSD and linux installation troubles"
date = "2019-09-20T21:00:50+02:00"
tags = ["amd", "ryzen"]
author = "morph027"
+++

---

**UPDATE 2019-12-29**: Re-installed the notebook (needed dual-boot w/ Windows for Bios Update) and everything was working this time without any specific paramater using the same installer ISO (Ubunto 18.04.3)

---

Unfortunately, i've spilled something on my notebook (Skylake HP Envy 13) which lead to an unusable keyboard *grmlfpl*

I was quite curious about the new generation of mobile Ryzen processors (very happy with my 1.st gen desktop Ryzen), so the replacement is a Acer Swift 3 14 with an AMD Ryzen 3500u (SF314-41-R8HZ), which caused some headache in the beginning as i was not able to boot any linux installer. After messing around with almost every kernel param back from the past (`noacpi`, `noapic`, `nomodeset`) and all other recommendations (`rcu_nocbs=0-7`, `processor.max_cstate=5`) it always printed something like `nvme ncme0: controller is down; will reset`. In the end, it was the [WD SSDs fault](https://community.wd.com/t/linux-support-for-wd-black-nvme-2018/225446) and `nvme_core.default_ps_max_latency_us=5500` did the trick!