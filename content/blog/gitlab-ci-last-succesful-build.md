+++
date = "2016-03-17"
tags = ["gitlab"]
title = "Gitlab: grab the last succesful build artifact from CI"

+++

Uo to now, we can't just download the latest succesful build of a project (see [#4768](https://gitlab.com/gitlab-org/gitlab-ce/issues/4768)). Therefore, i'm using the API to fetch an ordered list of all project builds in state _"success"_ and filter for the last one. Using this ID, we can download the artifact via API. For more details, check this [file](https://gitlab.com/morph027/gitlab-ci-helpers/blob/master/get-last-successful-build-artifact.sh). It needs some vars to work: ```BASE_URL PRIVATE_TOKEN PROJECT```, you'll find more info in the project [README](https://gitlab.com/morph027/gitlab-ci-helpers/blob/master/README.md).
