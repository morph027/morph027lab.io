+++
author = "morph027"
date = "2016-12-06T13:41:34+01:00"
description = ""
tags = ["gpg", "postfix"]
title = "Zeyple Postfix GPG Gateway"

+++

If you're using postfix as mail server or relay for your mails (e.g. log mails, ...) you might want to encrypt them.

[Zeyple does!](https://github.com/infertux/zeyple)
