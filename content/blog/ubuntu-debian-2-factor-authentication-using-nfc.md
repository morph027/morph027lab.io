+++
date = "2015-01-09"
tags = ["nfc", "2factor"]
title = "Ubuntu/Debian 2 factor authentication using nfc"

+++

At work, we are currently testing 2 factor authentication for desktops/workstations. As we are already using some nfc based cards, we want to use them. Some quick research brought up [pam_nfc](http://nfc-tools.org/index.php?title=Pam_nfc) which utilizes [libnfc](http://nfc-tools.org/index.php?title=Libnfc).

So we bought an ACR122U reader which is supported by *libnfc*

## compile and install

Checkout source code and create a debian package for further automated distribution (e.g. Puppet,...)

```
svn checkout http://nfc-tools.googlecode.com/svn/trunk/ nfc-tools-read-only 
```

Now we need to adjust some requirements for libraries if we're using Ubuntu as the debian control file requires *libnfc2 (>= 1.5.0)* which is not present in Ubuntu. Instead it's called *libnfc5* (*1.7.0-3* atm in trusty)

```
cd nfc-tools-read-only/pam_nfc
sed -i 's,libnfc2 (>= 1.5.0),libnfc5 (1.7.0-3),' debian/control
```

Next, install some packages and headers for compiling the source

```
sudo apt-get install devscripts autoconf dh-autoreconf build-essential fakeroot libnfc-dev libpam0g-dev -y
```

Finally, build the package

```
debuild -us -uc
```

Answer this question with yes as the package will be built:

```
> This package has a Debian revision number but there does not seem to be
> an appropriate original tar file or .orig directory in the parent directory;
> (expected one of libpam-nfc_0.1.4.orig.tar.gz, libpam-nfc_0.1.4.orig.tar.bz2,
> libpam-nfc_0.1.4.orig.tar.lzma,  libpam-nfc_0.1.4.orig.tar.xz or pam_nfc.orig)
> continue anyway? (y/n)
```

Now we've got a deb package to install one folder up.

## Installation

Grab the package built above and install it (or add it to your existing repo like freight or reprepro). This command might differ in version numbers and architecture and assumes you're still in the current *pam_nfc* directory. Adjust to your needs.

```
sudo apt-get install ../libpam-nfc_0.1.4-0_amd64.deb
```

## PAM configuration

Now we can add pam directives to the configuration files in */etc/pam.d/* to make it work. But in the past, we've encountered some problems with *pam-auth-update* in automated installations (via Puppet) so let's create a description file for *pam-auth-update* to have it working the clean way.

Create */usr/share/pam-configs/nfc* with content like this:

```
Name: NFC
Default: yes
Priority: 257
Auth-Type: Primary
Auth:
	required	pam_nfc.so
Auth-Initial:
	required	pam_nfc.so
```

This example is adding *pam_nfc.so* as required (so NFC tag and password will be asked) just before *pam_unix.so*. In our case, we are using Priority 251 so it skips local users (below *pam_unix.so*) but before *pam_lsass.so* (Active Directory using PowerBroker, you might not have this). If you want to use either username/password or nfc tag (ok, then it's no more 2 factor auth ;) ), just change *required* to *sufficient*.

## Enroll tags

Now we need to enroll tags for users, this is done via

```
pam-nfc-add <user>
```

Pairings will be put into */etc/pam_nfc.conf* which can be enrolled company wide.

## Testing

As always when testing PAM, keep an existing root shell to revert the settings in case it does not work.
