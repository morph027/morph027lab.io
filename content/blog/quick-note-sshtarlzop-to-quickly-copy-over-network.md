+++
date = "2016-02-24"
title = "Quick Note: ssh+tar+lzop to quickly copy over network"

+++

You need _lzop_ installed ;)

## from target host

```bash
ssh $user@$source-host tar --use-compress-program=lzop -cf - /source/folder | tar --use-compress-program=lzop -xf -
```
		
## from source host

```bash
cd /source/folder
tar --use-compress-program=lzop -cf - . | ssh $user@$target-host tar -C /target/folder --use-compress-program=lzop -xf -
```
