+++
date = "2013-06-27"
draft = false
tags = ["samba4"]
title = "Secured dynamic DNS updates with SAMBA4"

+++

*ISC DHCPd* is capable of sending dynamic dns updates against servers like *BIND* using shared-key authentication or or completely unauthenticated (e.g. BIND or Microsoft DNS with secure updates disabled). As *SAMBA4* is a great alternative for Microsoft AD, i'd like to perform secured dynamic DNS updates against it using [GSS/TSIG](https://en.wikipedia.org/wiki/Generic_Security_Service_Algorithm_for_Secret_Key_Transaction).

---
**UPDATE 2018-01-07**

* I'm now using the scripts from Arch Linux [samba-dhcpd-update](https://aur.archlinux.org/packages/samba-dhcpd-update/) package, for more info see the [wiki](https://wiki.archlinux.org/index.php/Samba/Active_Directory_domain_controller#DHCP_with_dynamic_DNS_updates)

**UPDATE 2016-02-17**

* Added info about ACLs for BIND

**UPDATE 2016-02-16**

* Added info about separate keytab file

---

## SAMBA4

Just follow the steps outlined in [Samba Wiki](https://wiki.samba.org/index.php/Configure_BIND_as_backend_for_Samba_AD#DNS_dynamic_updates_via_Kerberos_.28optional.2C_but_recommended.29) to get dynamic updates running. If your reverse zone does not already exist, just create it on your samba4 server:

samba-tool dns zonecreate localhost 1.168.192.in-addr.arpa -U administrator

Just note the reversed network subnet...

## ISC DHCPd

In your dhcp server configuration file (*dhcpd.conf*), you'll need the following lines:

```
on commit {
set ClientIP = binary-to-ascii(10, 8, ".", leased-address);
set ClientDHCID = binary-to-ascii(16, 8, ":", hardware);
set ClientName = pick-first-value(option host-name, host-decl-name);
execute("/opt/dhcp-dns/dhcp-dns.sh", "add", ClientIP, ClientDHCID, ClientName);
}

on release {
set ClientIP = binary-to-ascii(10, 8, ".", leased-address);
set ClientDHCID = binary-to-ascii(16, 8, ":", hardware);
execute("/opt/dhcp-dns/dhcp-dns.sh", "delete", ClientIP, ClientDHCID);
}
```

## nsupdate script

Get this [script](../content/dhcp-dns.sh) (md5 *1d47cc9f54c6a492b9224b3ece3bbbb1*) and make it executable:

```
mkdir -p /opt/dhcp-dns/
wget -O /opt/dhcp-dns/dhcp-dns.sh https://blog.morph027.de/content/dhcp-dns.sh
chmod +x dhcp-dns.sh
```

## keytab

At the time of writing this post, it did work using the automatically created keytab (_/usr/local/samba/private/dns.keytab_). Now (2016-02-16), i try to setup a fresh DC with samba 4.2.8-8 and it refuses to work. So i just create a dedicated keytab file for the script:

```
samba-tool domain exportkeytab /opt/dhcp-dns/dhcp-dns.keytab --principal=dns-dc1@YOUR.REALM.COM
samba-tool domain exportkeytab /opt/dhcp-dns/dhcp-dns.keytab --principal=DNS/dc1.your.domain.com@YOUR.REALM.COM
```

## BIND

### ACL

To make sure bind can read the neccessary files, i've applied these ACLs:
  
```
setfacl -m "u:bind:r" /var/lib/samba/private/dns.keytab
setfacl -m "u:bind:rx" /var/lib/samba/private
setfacl -m "u:bind:rx" /var/lib/samba
setfacl -m "u:bind:rx" /var/lib/samba/private/dns
setfacl -m "u:bind:rx" /var/lib/samba/private/dns/sam.ldb.d/
setfacl -m "u:bind:rw" /var/lib/samba/private/dns/sam.ldb.d/*.ldb 
setfacl -m "u:bind:rw" /var/lib/samba/private/dns/sam.ldb.d/metadata.tdb 
setfacl -m "u:bind:r" /var/lib/samba/private/named.conf
setfacl -m "u:bind:rw" /var/lib/samba/private/dns/sam.ldb
```

### References

* [blog.michael.kuron-germany.de](http://blog.michael.kuron-germany.de/2011/02/isc-dhcpd-dynamic-dns-updates-against-secure-microsoft-dns/)
