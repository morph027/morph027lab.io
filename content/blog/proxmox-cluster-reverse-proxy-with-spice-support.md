+++
date = "2014-02-04"
tags = ["proxmox"]
title = "Proxmox Cluster Reverse Proxy with Spice support"

+++

Having a cluster of Proxmox nodes is just fine, but for some reasons (unified web+api access, signed certificates) we want to have a reverse proxy in front of our cluster members. We're using Apache2 as HTTPS load balancer and [HAProxy](http://haproxy.1wt.eu/) as TCP load balancer for Spice/VNC sessions.

Basic Proxmox Reverse Proxy config can be found at [James Coyle's blog](http://www.jamescoyle.net/how-to/923-reverse-proxy-proxmox-with-apache) including hints how to setup Apache2.

To cover a cluster of multiple nodes, you need to add the *balancer* section.

```apache2
<Proxy balancer://proxmox>
	BalancerMember https://node1:8006
	BalancerMember https://node2:8006
	BalancerMember https://node3:8006
	BalancerMember https://node4:8006
</Proxy>

<VirtualHost *:443>
	SSLEngine On
	SSLCertificateFile /etc/apache2/ssl/proxmox.cer
	SSLCertificateKeyFile /etc/apache2/ssl/proxmox.key
	SSLProxyEngine on
	SSLProxyVerify none

<Location>
	ProxyPass / balancer://proxmox/
	ProxyPassReverse / balancer://proxmox/
	Order allow,deny
	Allow from all
</Location>

</VirtualHost>
```

To allow some of our existing API scripts to connect on default port 8006, we just put the same config into another *VirtualHost* section:

```apache2
<VirtualHost *:8006>
	SSLEngine On
	SSLCertificateFile /etc/apache2/ssl/proxmox.cer
	SSLCertificateKeyFile /etc/apache2/ssl/proxmox.key
	SSLProxyEngine on
	SSLProxyVerify none

<Location>
	ProxyPass / balancer://proxmox/
	ProxyPassReverse / balancer://proxmox/
	Order allow,deny
	Allow from all
</Location>

</VirtualHost>
```

Right now, the webinterface and API access works, but Spice won't as it's using port 3128 on every node.

This is our basic HAProxy config file:

```
# this config needs haproxy-1.1.28 or haproxy-1.2.1

global
	log 127.0.0.1	local0
	#log 127.0.0.1	local1 notice
	maxconn 4096
	#chroot /usr/share/haproxy
	user haproxy
	group haproxy
	daemon
	#debug
	#quiet
	stats socket /var/run/haproxy/haproxy.sock mode 0644 uid 0 gid 107

defaults
	log			global
	mode		tcp
	option		tcplog
	option		dontlognull
	retries		3
	option 		redispatch
	maxconn		2000
	contimeout	5000
	clitimeout	50000
	srvtimeout	50000

listen proxmox_spice *:3128
	mode tcp
	option tcpka
	balance roundrobin
	server node1 xxx.xxx.xxx.xxx:3128 weight 1
	server node2 xxx.xxx.xxx.xxx:3128 weight 1
	server node3 xxx.xxx.xxx.xxx:3128 weight 1
	server node4 xxx.xxx.xxx.xxx:3128 weight 1

listen proxmox_vnc *:5900-5999
	mode tcp
	option tcpka
	balance roundrobin
	server node1 xxx.xxx.xxx.xxx:5900-5999 weight 1
	server node2 xxx.xxx.xxx.xxx:5900-5999 weight 1
	server node3 xxx.xxx.xxx.xxx:5900-5999 weight 1
	server node4 xxx.xxx.xxx.xxx:5900-5999 weight 1
```
