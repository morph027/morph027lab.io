+++
author = "morph027"
date = "2015-03-19T11:41:36+01:00"
title = "ZFS on Linux vs. LSI CacheCade: quick sysbench"
tags = ["zfs"]

+++

Quick shot on one of our servers (2 x E5-2667 v2 @ 3.30GHz, 256GB RAM, 6 x HGST HUS156060VLS600 15K SAS drives, 2 x mirrored 80GB Intel DC S3500 as cache for both tests)

Disk access is done using a BBU backed LSI is a *LSI MegaRAID SAS 9271-4i* utilizing both SSDs at RAID1 as R/W cache and a *IBM ServeRAID M1015* cross flashed to *LSI 9211-8i* in IT mode for the ZFS tests.

[sysbench](https://launchpad.net/sysbench) was using a 256MB and a 16GB testfile, test visualization is done with slightly modified version of [tsuna's sysbench-tools](https://github.com/tsuna/sysbench-tools) (needed to remove the ```--file-extra-flags=direct``` param in *runtests.sh* during zfs tests, also just one test file instead of 64)

You can select your own metrics in the [interactive version](/sysbench.html)

![Benchmark](/sysbench_lsi_zfs.png)
