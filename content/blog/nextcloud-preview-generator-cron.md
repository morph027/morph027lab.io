+++
title = "Setup cron or systemd timers for Nextcloud preview generator app"
description = ""
author = "morph027"
tags = ["nextcloud"]
date = "2018-07-27T08:00:00+02:00"
+++

If you're using the fantastic Nextcloud and like to feel comfortable browsing all your auto-uploaded photos like once upon a time you did in some proprietary Picasa or so, you can benefit from [Preview Generator](https://apps.nextcloud.com/apps/previewgenerator), which will take care of pre-rendering the thumbnails.

As it states you must add some cron magic to get it working and the Nextcloud folks asked for a tutorial, here's how to do it with cron or (my preference) with systemd timers.

## Prerequisites

* we need to setup the job as the system user running Nextcloud (e.g. `www-data`, `nginx`, `httpd`, ... - depends on your distribution). You can find out by looking at the owner of the nextcloud files using `ls -la`
* we need to use the absolut path to the `php` executable (`which php`)
* we need to use the absolut path to Nextclouds `occ` file, depends in which folder your Nextcloud has been installed (e.g. `/var/www/nextcloud/occ`)

## Cron

Add or create a new cronjob for the Nextcloud user:

```bash
crontab -e -u www-data
```

Using your favourite editor, add something like this, which will run the job at 04:00:

```
0 4 * * * /usr/bin/php -f /var/www/nextcloud/occ preview:pre-generate
```

## Systemd timers

Systemd also has the possibility to run specific tasks at specific times or events. called [Timers](https://wiki.archlinux.org/index.php/Systemd/Timers). You need to create 2 unit files:

### `/etc/systemd/system/nextcloud-preview-generator.service`

```
[Unit]
Description=Nextcloud Preview Generator

[Service]
Type=oneshot
User=www-data
ExecStart=/usr/bin/php -f /var/www/nextcloud/occ preview:pre-generate

[Install]
WantedBy=basic.target
```

### `/etc/systemd/system/nextcloud-preview-generator.timer`

```
[Unit]
Description=Run Nextcloud Preview Generator daily at 04:00

[Timer]
OnCalendar=*-*-* 4:00:00
Persistent=true

[Install]
WantedBy=timers.target
```

* systemd reload `systemctl daemon-reload`
* activate the timer `systemctl enable nextcloud-preview-generator.timer`
* start the timer `systemctl start nextcloud-preview-generator.timer`
