+++
author = "morph027"
date = "2016-12-14T14:55:07+01:00"
description = ""
tags = []
title = "Detect ransomware with cryptostalker"

+++

![](/img/stalker.jpg)

As an experienced linux only user, i could have ignored the latest waves of crypto malware. But i'm also taking care of some people being forced to use Windows and Office and so on.

Can't say enough:

* Backup!
* Backup!
* Backup!

If your happily using ZFS based sytems with high snapshot rate like me, you might be [quite relaxed](https://www.ixsystems.com/blog/defeating-cryptolocker/) about backup because of read-only snapshots.

But i also want to get noticed, when some bogus I/O starts on these servers. I was playing arounf with inotify and custom scripts, which just extracts the filename extension and shout out loud, when there are lots of files having the same (unknown before) extension. Works. But also, i stumbled upon [cryptostalker](https://github.com/unixist/cryptostalker), which more or less does the same (ok, it checks for encrypted files ;) ).

Nice, i created a [systemd unit](https://github.com/morph027/cryptostalker/tree/master/contrib/systemd) ([PR](https://github.com/unixist/cryptostalker/pull/18)) for my samba servers and implemented a [script feature](https://github.com/morph027/cryptostalker/tree/hook-script/contrib/scripts) ([PR](https://github.com/unixist/cryptostalker/pull/16)) to send some mail and [signal messages](https://github.com/morph027/signal-gateway).
